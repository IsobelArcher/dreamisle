﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    Animator thisanimator;

	// Use this for initialization
	void Start () {
        thisanimator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("w"))
        {
            thisanimator.SetTrigger("cubetestanimation");
        }
	}
}
